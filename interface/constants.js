// @ts-check

// BEGIN Typedefs

/** @typedef {typeof initDefaults} QRData */

/** @typedef {{x: number, y: number}} Point */

/** @typedef {(Bright | Dark | Unset)} PixelState */

/** @typedef {{state: PixelState, point: Point}} Pixel */

// END Typedefs

// BEGIN Constants

export const Bright = 0;
export const Dark = 1;
export const Unset = -1;

export const initDefaults = {
  version: 5,
  mask: 0,
  quality: 1,
  data: "Hello World!",
  rotation: 0,
};

// END Constants
// vi: foldmarker=BEGIN,END foldmethod=marker
