// @ts-check

import { Bright, Dark, Unset } from "./constants.js";
import { serialize } from "./serialization.js";
import {
  addPixel,
  cellId,
  download,
  drawCell,
  drawMultiplePixels,
  getRequiredElement,
  indicateLoading,
  onClick,
  onClickSlow,
  parsePixelState,
  setCellClasses,
} from "./utilities.js";

// BEGIN Type imports
// A bit verbose, but it's either this or using the import everywhere
//
/** @typedef {import("./constants.js").QRData} QRData */
/** @typedef {import("./constants.js").Pixel} Pixel */
/** @typedef {import("./constants.js").Point} Point */
/** @typedef {import("./constants.js").PixelState} PixelState */
/** @typedef {import("./qr_code.js").default} QRCode */
// END Type imports

/** @typedef {{ version: () => HTMLInputElement, quality: () => HTMLSelectElement, mask: () => HTMLInputElement, data: () => HTMLInputElement, method: () => HTMLSelectElement }} Inputs */
/** @typedef {{qr: QRCode, drawnPixels: PixelState[][], chosenColor: PixelState}} State */

/**
 * @param {PointerEvent} pointerEvent
 * @param {PixelState} chosenColor
 * @returns {PixelState | null}
 */
function extractState(pointerEvent, chosenColor) {
  switch (pointerEvent.buttons) {
    case 1:
      if (pointerEvent.shiftKey) {
        return Bright;
      }
      if (pointerEvent.ctrlKey) {
        return Unset;
      }
      return chosenColor;

    case 2:
      return Bright;
    case 4:
      return Unset;
    default:
      return null;
  }
}
/**
 * @param {State} state
 * @param {PointerEvent} pointerEvent
 * @param {Point} point
 */
function cellClick({ qr, drawnPixels, chosenColor }, pointerEvent, point) {
  const translated = extractState(pointerEvent, chosenColor);

  if (translated === null) {
    return;
  }

  /** @type {Element} */
  (pointerEvent.target).releasePointerCapture(pointerEvent.pointerId);
  pointerEvent.preventDefault();

  if (qr.setPixel({ point: point, state: translated })) {
    const [changedX, changedY] = qr.changeCoords(point);
    const changedPoint = { x: changedX, y: changedY };
    addPixel(drawnPixels, { point: changedPoint, state: translated });
    qr.forEach(drawCell);
  }
}
/**
 * @param {State} state
 */
function initQRTable(state) {
  const { qr } = state;
  const table = getRequiredElement("qr-table");

  table.addEventListener("contextmenu", (ev) => ev.preventDefault(), true);

  /** @type {HTMLElement[]} */
  const newChildren = [];

  qr.forEach(({ state: pixelState, point }) => {
    const cell = document.createElement("div");
    cell.id = cellId(point);
    cell.classList.add("qr-cell");

    cell.addEventListener(
      "pointerdown",
      (ev) => cellClick(state, ev, point),
      true,
    );
    cell.addEventListener(
      "pointerenter",
      (ev) => cellClick(state, ev, point),
      true,
    );

    setCellClasses(cell, pixelState);
    newChildren.push(cell);
  });

  table.replaceChildren(...newChildren);
}

/**
 * @param {State} state
 * @param {Inputs} inputs
 */
function apply(state, inputs) {
  const { qr, drawnPixels } = state;
  qr.reinit({
    version: inputs.version().valueAsNumber,
    quality: Number(inputs.quality().value),
    mask: inputs.mask().valueAsNumber,
    data: inputs.data().value,
  });

  const newPixels = drawMultiplePixels(
    drawnPixels.flatMap((column, x) =>
      column.map((state, y) => ({ point: { x, y }, state })),
    ),
    qr,
  );

  // A direct splice will introduce undefineds that are handled differently than empty slots...
  drawnPixels.splice(0);
  newPixels.forEach((column, x) => {
    drawnPixels[x] = column;
  });

  redrawQRCode(state);
}
/**
 * @param {State} state
 */
function redrawQRCode(state) {
  initQRTable(state);
  document.documentElement.style.setProperty("--qr-size", `${state.qr.size}`);
}

/**
 * @param {QRCode} qr
 * @param {string} method
 */
function fill(qr, method) {
  qr.fill(method);
  qr.forEach(drawCell);
}

// BEGIN Event listeners

/**
 * @param {QRCode} qr
 * @param {PixelState[][]} drawnPixels
 * @param {Inputs} inputs
 */
export function setupEventListeners(qr, drawnPixels, inputs) {
  const /** @type {State} */ state = { qr, drawnPixels, chosenColor: Dark };

  onClickSlow("qr-reset-button", () => {
    qr.initData.rotation = 0;
    state.drawnPixels = [];
    window.history.replaceState(
      {},
      document.title,
      new URL(document.URL).pathname,
    );

    apply(state, inputs);
  });

  onClick("qr-rotate-button", () => {
    qr.initData.rotation = (qr.initData.rotation + 1) % 4;
    qr.forEach(drawCell);
  });

  onClickSlow("qr-fill-button", () => fill(qr, inputs.method().value));

  onClick("qr-share-button", () => {
    const curUrl = new URL(document.URL);
    const newUrl = new URL(curUrl.pathname, curUrl);

    serialize(qr, drawnPixels, newUrl.searchParams);

    alert(`Copy the following link to share your work:\n${newUrl.href}`);
  });

  onClickSlow("qr-download-button", () => {
    const squareSize = 10;
    const canvas = document.createElement("canvas");

    canvas.height = qr.size * squareSize;
    canvas.width = qr.size * squareSize;

    const ctx = canvas.getContext("2d");
    if (!ctx) {
      throw new Error("Canvas has no context");
    }

    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "black";

    fill(qr, inputs.method().value);
    qr.forEach(({ state, point: { x, y } }) => {
      if (state === Dark) {
        ctx.fillRect(x * squareSize, y * squareSize, squareSize, squareSize);
      } else if (state !== Bright) {
        console.warn(
          `QR code is exported but pixel at row ${y} and column ${x} is neither bright nor dark but: ${state}`,
        );
      }
    });

    download(canvas.toDataURL(), `${encodeURIComponent(qr.initData.data)}.png`);
  });

  onClick("qr-svg-download-button", () => {
    const squareSize = 10;
    const svgNS = "http://www.w3.org/2000/svg";
    const svg = document.createElement("svg");

    const size = qr.size * squareSize;

    svg.setAttribute("viewBox", `0 0 ${size} ${size}`);
    svg.setAttribute("width", `${size}`);
    svg.setAttribute("height", `${size}`);
    svg.setAttribute("fill", "white");
    svg.setAttribute("xmlns", svgNS);
    // Disable antialising since that might introduce faint lines between rectangles
    svg.setAttribute("shape-rendering", "crispEdges");

    const pixelGroup = document.createElementNS(svgNS, "g");
    pixelGroup.setAttribute("fill", "black");

    // Helps against faint lines between rectangles
    pixelGroup.setAttribute("stroke", "black");
    pixelGroup.setAttribute("stroke-width", "0.1");

    svg.appendChild(pixelGroup);

    fill(qr, inputs.method().value);
    qr.forEach(({ state, point: { x, y } }) => {
      if (state === Dark) {
        const rect = document.createElementNS(svgNS, "rect");
        rect.setAttribute("x", `${x * squareSize}`);
        rect.setAttribute("y", `${y * squareSize}`);
        rect.setAttribute("width", squareSize.toString());
        rect.setAttribute("height", squareSize.toString());

        pixelGroup.appendChild(rect);
      } else if (state !== Bright) {
        console.warn(
          `QR code is exported but pixel at row ${y} and column ${x} is neither bright nor dark but: ${state}`,
        );
      }
    });

    const payload = "data:image/svg+xml," + encodeURIComponent(svg.outerHTML);
    download(payload, `${encodeURIComponent(qr.initData.data)}.svg`);
  });

  for (const radio of /** @type {NodeListOf<HTMLInputElement>} */ (
    document.querySelectorAll("input[type=radio][name=color-choice]")
  )) {
    if (radio.checked) {
      state.chosenColor = parsePixelState(radio.value);
    }
    radio.addEventListener("change", (ev) => {
      const elem = /** @type {HTMLInputElement} */ (ev.target);
      if (elem.checked) {
        state.chosenColor = parsePixelState(elem.value);
      }
    });
  }

  getRequiredElement("config-form").addEventListener(
    "submit",
    (ev) => {
      ev.preventDefault();
      ev.stopPropagation();
      indicateLoading(() => apply(state, inputs), "qr-apply-button");
    },
    true,
  );

  redrawQRCode(state);
}

// END Event listeners

// vi: foldmarker=BEGIN,END foldmethod=marker
