// @ts-check

import createModule from "../application/qr_wasm.js";
import { Dark } from "./constants.js";
import { setupEventListeners } from "./interface.js";
import { deserialize } from "./serialization.js";
import { getRequiredElement } from "./utilities.js";

// BEGIN Type imports
// A bit verbose, but it's either this or using the import everywhere
/** @typedef {import("./constants.js").QRData} QRData */
/** @typedef {import("./constants.js").Pixel} Pixel */
/** @typedef {import("./constants.js").Point} Point */
/** @typedef {import("./constants.js").PixelState} PixelState */
//
// END Type imports

const inputs = {
  version: () =>
    /** @type {HTMLInputElement} */ (getRequiredElement("qr-version")),
  quality: () =>
    /** @type {HTMLSelectElement} */ (getRequiredElement("qr-quality")),
  mask: () => /** @type {HTMLInputElement} */ (getRequiredElement("qr-mask")),
  data: () => /** @type {HTMLInputElement} */ (getRequiredElement("qr-data")),
  method: () =>
    /** @type {HTMLSelectElement} */ (getRequiredElement("qr-fill-type")),
};

// BEGIN Initialization code

const module = await createModule();

const {qr, drawnPixels} = deserialize(
  module,
  inputs,
  new URLSearchParams(new URL(document.URL).search),
);

getRequiredElement("wasm-loading").remove();

setupEventListeners(qr, drawnPixels, inputs);

// END Initialization code

// vi: foldmarker=BEGIN,END foldmethod=marker
