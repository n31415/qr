// @ts-check

import { Bright, Dark, Unset } from "./constants.js";

// BEGIN Type imports
// A bit verbose, but it's either this or using the import everywhere
/** @typedef {import("./constants.js").Pixel} Pixel */
/** @typedef {import("./constants.js").Point} Point */
/** @typedef {import("./constants.js").PixelState} PixelState */
//
// END Type imports

/**
 * @template T
 * @param {() => T} func
 * @param {string} indicatingElementId
 * @returns {Promise<T>}
 */
export async function indicateLoading(func, indicatingElementId) {
  const elem = getRequiredElement(indicatingElementId);
  elem.classList.add("loading");

  return runAsync(func).finally(() => elem.classList.remove("loading"));
}

/**
 * @param {string} filename
 * @param {string} payload
 */
export function download(payload, filename) {
  const link = document.createElement("a");
  link.download = filename;
  link.href = payload;
  link.click();
  link.remove();
}

/**
 * @param {PixelState[][]} twoDArray
 * @param {Pixel} param1
 */
export function addPixel(twoDArray, { point: { x, y }, state }) {
  if (state === Unset) {
    if (twoDArray[x]) {
      delete twoDArray[x][y];
    }

    return;
  }

  if (twoDArray[x] === undefined) {
    twoDArray[x] = [];
  }

  twoDArray[x][y] = state;
}
/**
 * @template T
 * @param {() => T} func
 * @returns {Promise<T>}
 */
function runAsync(func) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      try {
        resolve(func());
      } catch (e) {
        reject(e);
      }
    }, 0);
  });
}

/**
 * @param {string} id
 * @returns {HTMLElement}
 */
export function getRequiredElement(id) {
  const elem = document.getElementById(id);
  if (elem === null) {
    throw new Error(
      `DOM element with id ${id} does not (yet) exist, but it is mandatory!`,
    );
  }

  return elem;
}

/**
 * @param {string} id
 * @param {(this: HTMLElement, ev: MouseEvent) => any} listener
 */
export function onClick(id, listener) {
  getRequiredElement(id).addEventListener("click", listener);
}

/**
 * @param {string} id
 * @param {(ev: MouseEvent) => any} listener
 */
export function onClickSlow(id, listener) {
  getRequiredElement(id).addEventListener(
    "click",
    (/** @type {MouseEvent} */ ev) => indicateLoading(() => listener(ev), id),
  );
}

/**
 * @param {string} arg
 * @returns {PixelState}
 */
export function parsePixelState(arg) {
  switch (arg) {
    case "Dark":
      return Dark;
    case "Bright":
      return Bright;
    case "Unset":
      return Unset;
    default:
      throw new Error(`string "${arg}" is no valid PixelState`);
  }
}

/**
 * @param {Point} param0
 */
export function cellId({ x, y }) {
  return `qr-cell-${x}-${y}`;
}

/**
 * @param {Pixel[]} toDraw
 * @param {import("./interface.js").QRCode} qr
 * @returns {PixelState[][]}
 */
export function drawMultiplePixels(toDraw, qr) {
  const num_synced = qr.setMultiplePixels(toDraw);

  const drawn = [];

  toDraw.slice(0, num_synced).forEach((px) => addPixel(drawn, px));

  return drawn;
}
/**
 * @param {HTMLElement} cell
 * @param {PixelState} state
 */
export function setCellClasses(cell, state) {
  switch (state) {
    case Bright:
      cell.classList.add("bright");
      cell.classList.remove("dark");
      break;
    case Dark:
      cell.classList.add("dark");
      cell.classList.remove("bright");
      break;
    default:
      cell.classList.remove("bright");
      cell.classList.remove("dark");
      break;
  }
}

/**
 * @param {Pixel} param0
 */
export function drawCell({ state, point }) {
  setCellClasses(getRequiredElement(cellId(point)), state);
}
// END Helper functions
// vi: foldmarker=BEGIN,END foldmethod=marker
