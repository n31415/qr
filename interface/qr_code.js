// @ts-check

// BEGIN Type imports
// A bit verbose, but it's either this or using the import everywhere
/** @typedef {import("./constants.js").QRData} QRData */
/** @typedef {import("./constants.js").Pixel} Pixel */
/** @typedef {import("./constants.js").Point} Point */
/** @typedef {import("./constants.js").PixelState} PixelState */
/** @typedef {import("../application/qr_wasm.js").Module} Module */
//
// END Type imports

// BEGIN Constants

// END Constants

export default class QRCode {
  /** @type {QRData} */
  initData;

  /** @type {number} */
  qrPtr;

  /** @type {number} */
  size;

  /**
   * @param {Module} module
   * @param {QRData} initData
   */
  constructor(module, initData) {
    this.module = module;
    this.#init(initData);
  }

  /**
   * @param {QRData} initData
   */
  #init(initData) {
    this.initData = initData;
    this.qrPtr = this.module.ccall(
      "init_qr",
      "number",
      ["number", "number", "number", "string"],
      [
        this.initData.version,
        this.initData.quality,
        this.initData.mask,
        this.initData.data,
      ],
    );
    if (!this.qrPtr) {
      alert("Invalid QR code config!");
    }
    this.size = this.module._get_size(this.qrPtr);
  }

  /**
   * @param {Partial<QRData>} initData
   */
  reinit(initData) {
    this.module._destroy_qr(this.qrPtr);
    this.#init({ ...this.initData, ...initData });
  }

  /**
   * @param {function(Pixel): *} func
   */
  forEach(func) {
    for (let rowIndex = 0; rowIndex < this.size; ++rowIndex) {
      for (let colIndex = 0; colIndex < this.size; ++colIndex) {
        func(this.#getPixel({ x: colIndex, y: rowIndex }));
      }
    }
  }

  /**
   * @param {string} method
   */
  fill(method) {
    const result = this.module.ccall(
      "fill",
      "number",
      ["number", "string"],
      [this.qrPtr, method],
    );
    if (!result) {
      throw new Error(`Unknown fill method ${method}`);
    }
  }

  /**
   * @param {Pixel} px
   */
  setPixel({ state, point }) {
    return this.module._set_pixel(
      this.qrPtr,
      state,
      ...this.changeCoords(point),
    );
  }

  /**
   * @param {Point} point
   * @returns {Pixel}
   */
  #getPixel(point) {
    return {
      state: /** @type {PixelState} */ (
        this.module._get_pixel(this.qrPtr, ...this.changeCoords(point))
      ),
      point,
    };
  }

  /**
   * @param {Pixel[]} toDraw
   * @returns {number}
   */
  setMultiplePixels(toDraw) {
    const packed = new Uint8Array(
      toDraw.flatMap(({ point: { x, y }, state }) => [x, y, state]),
    );

    return this.module.ccall(
      "draw_many_pixels",
      "number",
      ["number", "number", "array"],
      [this.qrPtr, toDraw.length, packed],
    );
  }

  /**
   * @param {Point} param0
   * @returns {[number, number]}
   */
  changeCoords({ x, y }) {
    switch (this.initData.rotation % 4) {
      case 1:
        return [y, this.size - x - 1];
      case 2:
        return [this.size - x - 1, this.size - y - 1];
      case 3:
        return [this.size - y - 1, x];
      default:
        return [x, y];
    }
  }
}
