// @ts-check

import { Bright, Dark, initDefaults } from "./constants.js";
import QRCode from "./qr_code.js";
import { drawMultiplePixels } from "./utilities.js";

// BEGIN Type imports
// A bit verbose, but it's either this or using the import everywhere
/** @typedef {import("./constants.js").QRData} QRData */
/** @typedef {import("./constants.js").Pixel} Pixel */
/** @typedef {import("./constants.js").Point} Point */
/** @typedef {import("./constants.js").PixelState} PixelState */
//
// END Type imports

/**
 * @param {string} base64
 * @yields {Point}
 */
function* base64ToPoints(base64) {
  const binaryString = atob(base64);
  for (let i = 0; i < binaryString.length; i += 2) {
    const x = binaryString.charCodeAt(i);
    const y = binaryString.charCodeAt(i + 1);
    yield { x, y };
  }
}

/**
 * @param {Point[]} points
 * @returns {string}
 */
function pointsToBase64(points) {
  const base64 = btoa(
    String.fromCharCode(...points.flatMap(({ x, y }) => [x, y])),
  );

  let lastEqual = base64.length - 1;
  while (base64[lastEqual] === "=") {
    lastEqual--;
  }

  return base64.substring(0, lastEqual + 1);
}

/**
 * @param {QRCode} qr
 * @param {PixelState[][]} drawnPixels
 * @param {URLSearchParams} paramsObject
 */
export function serialize(qr, drawnPixels, paramsObject) {
  Object.entries(qr.initData).forEach(([key, value]) =>
    paramsObject.append(key, value.toString()),
  );

  /** @type {{0:Point[], 1: Point[]}} */
  const points = { [Bright]: [], [Dark]: [] };

  drawnPixels.forEach((column, x) =>
    column.map((state, y) => {
      points[state].push({ x, y });
    }),
  );

  paramsObject.append("dark", pointsToBase64(points[Dark]));
  paramsObject.append("bright", pointsToBase64(points[Bright]));

  return paramsObject;
}

/**
 * @param {import("../application/qr_wasm.js").Module} module
 * @param {{[x:string]: () => (HTMLInputElement|HTMLSelectElement)}} inputs
 * @param {URLSearchParams} params
 */
export function deserialize(module, inputs, params) {
  /** @type {Pixel[]} */
  const toBeDrawn = [];
  const data = { ...initDefaults };

  for (const [key, value] of params) {
    if (key === "bright" || key === "dark") {
      const state = key === "dark" ? Dark : Bright;
      for (const { x, y } of base64ToPoints(value)) {
        toBeDrawn.push({ point: { x, y }, state });
      }
    } else if (key in initDefaults) {
      if (typeof initDefaults[key] === "number") {
        data[key] = Number(value);
      } else {
        data[key] = value;
      }
    } else {
      console.warn(`I don't know what to do with ${key}=${value}`);
    }
  }

  for (const key in inputs) {
    if (key in data) {
      inputs[key]().value = data[key];
    }
  }

  const qr = new QRCode(module, data);
  const drawnPixels = drawMultiplePixels(toBeDrawn, qr);

  return { qr, drawnPixels };
}
