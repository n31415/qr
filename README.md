# Draw your own QR codes!

This project allows you to draw your own QR codes. You can specify the content, size and error correction you want. Then, there normally are still some pixels left that you can set as you want. In the end you get a QR code that is 100% valid (we are not abusing the error correction here) and looks like you want it to.

## How does it work?

This is based on work by [https://math.mp42.de/](Matthias), who wrote the initial C script. Normally, the data you want a QR code to contain does not fill its whole content. The QR code has a length property such that only the beginning part of its content is considered. We can then fill the rest with whatever binary garbage you want.

However, that is not the only thing determining the pixels. There is also some Reed-Solomon error correction baked into the QR code. You basically compute the error correction bits for your data and translate the data bits and the error correction bits into pixels. This process can also be reversed by solving some linear equations. So you start with the pixels you want and get the necessary content you need to achieve this data, starting with the content you provided and ending with some garbage.

Since the error correction is necessarily determined by the content, there is some redundancy in the QR code which causes the pixels to not be completely free. In other words, drawing a specific pixel might sometimes force another pixel to be set a certain way for the error correction to work.

## Examples

<div style="display: flex; flex-wrap: wrap; padding-inline: 2em; gap: 1em; justify-content: space-between;">

[![A QR code for the QED maths club](qed.png "QED maths club"){style="border: 1em solid white;"}](https://qed-verein.de)

[![A QR code for the club organizing the Maths Olympiad in Bavaria](moby.png "MOBy maths club"){style="border: 1em solid white;"}](https://mo-by.de)

[![A QR code to the website creating these](qr.png "Very meta"){style="border: 1em solid white;"}](https://nicholas-schwab.de/qr.html)

</div>

## How can I use it?

It is deployed on [my website](https://nicholas-schwab.de/qr.html).

If you want to run it locally, that only works on Linux and you need the `gdk-3.0` library installed. If that is the case `make app` should generate you an executable named `qr`. That can then be run as

```
./qr <version> <quality> <mask> <data>
```

The first three arguments are options on the QR code. Here, `version` is a number between 1 and 40 and determines the size. The other two numbers are a bit technical. Setting them to 1 and 0 should work. The last argument is the content which the QR code should have. Then, a window should open where you can draw your QR code.

Use the left mouse button for drawing black, the right one for drawing white and the middle for unsetting a pixel. Right now, that has no option to fill the QR code, so you need to set pixels until it is completely filled. There is also no way to save it, so you need to make a screenshot. The web interface has a better usability.

## How can I hack on it?

You probably need Linux to work with this project.
At least is it not tested on any other system.

### C Code

For the local application you just need a C compiler and the [`gdk-3`](https://docs.gtk.org/gdk3/) library.
The files are all in the `application/` directory and have roughly the following purpose

 * `main.c`: Entry point and setup of key handlers
 * `application.c`: Setup of gdk window
 * `qr.c`: QR Code logic and interface
 * `rs.c`: Reed-Solomon logic and linear equation solver

### WASM Code

To compile the WASM application, you need [`emscripten`](https://emscripten.org/docs/introducing_emscripten/index.html).

Most of the logic is shared between the local and the WASM application.
Both depend on `qr.c` and `rs.c` and use them in the same way.
The WASM application additionally depends on `qr_wasm.c` which specifies the WASM-interfaces that can be used by the JavaScript code.
It is only a pretty thin wrapper around the functions declared in `qr.h`.

Further, there is a script to generate JSDoc types from the `emscripten` interface functions, though that is not necessary and implemented a bit haphazardly.

### JavaScript Code

In the `interface/` directory you find a few JavaScript files that are the glue between the WASM code and the HTML interface. They react on config changes, drawing of pixels, and so on and set pixels if specified by the logic in the WASM code. The JavaScript is annotated with JSDoc types to give type checking while not needing a build step.

Since there is no need for a build step, you can just serve the root directory of this repo via http and visit `qr.html` and everything should work. For my deployment, I also have a setup to bundle the frontend code with `vite`, but this is not strictly necessary.

### Makefile

There is a Makefile that builds both the gdk application and the http frontend if you just run `make all`. For developing on the latter, there are also two convenient targets. `serve` will just serve the current directory via the python basic http server and open the `qr.html` in the default browser. `ci` does some quick and dirty continuous integration by running make whenever a file in the repo changes (though that is only needed if you change something with the C code). There is no automatic reload and I suggest you disable caching for this page when you work on the JavaScript components.
