NAME = qr
APPLICATION_DIR = application
PROG = $(APPLICATION_DIR)/$(NAME)
BROWSER = xdg-open

WASM_NAME = qr_wasm
PROG_JS = $(APPLICATION_DIR)/$(WASM_NAME).js
WASM_D_TS = $(APPLICATION_DIR)/$(WASM_NAME).d.ts
PROG_WASM = $(APPLICATION_DIR)/$(WASM_NAME).wasm

INTERFACE_DIR = interface
HTML_MAIN = $(NAME).html
TO_BE_DEPLOYED = $(HTML_MAIN) $(PROG_JS) $(PROG_WASM) $(INTERFACE_DIR)/interface.js
DEPLOY_DIR = .deploy
SERVER ?=
REMOTE_DIR ?=
ASSETS_DIR = qr_assets
DEPLOY_DIR_DUMMY_FILE = $(DEPLOY_DIR)/.dummy
OLD_DEPLOYABLE_PATTERNS=$(subst $(DEPLOY_DIR),$(REMOTE_DIR),$(subst $(HASH),*,$(DEPLOYABLES)))

.SUFFIXES:

.PHONY: all app clean wasm deploy ci serve

all: app wasm compile_flags.txt

app: $(PROG)

wasm: $(PROG_JS) $(WASM_D_TS)

deploy: $(DEPLOY_DIR_DUMMY_FILE)

$(DEPLOY_DIR_DUMMY_FILE): $(TO_BE_DEPLOYED)
ifndef SERVER
	$(error SERVER is not defined)
endif
ifndef REMOTE_DIR
	$(error REMOTE_DIR is not defined)
endif

	vite build --outDir $(DEPLOY_DIR) --assetsDir $(ASSETS_DIR)

	ssh $(SERVER) sh -c '"rm -rf $(REMOTE_DIR)/$(ASSETS_DIR)"'
	scp -r $(DEPLOY_DIR)/$(NAME).html $(DEPLOY_DIR)/$(ASSETS_DIR)/ $(SERVER):$(REMOTE_DIR)

	touch $(DEPLOY_DIR_DUMMY_FILE)

application/%:
	$(MAKE) -C $(APPLICATION_DIR) $(@F)

serve:
	$(BROWSER) http://localhost:8000/qr.html &
	python -m http.server

ci:
	git ls-tree -r HEAD --name-only | entr make

clean:
	$(MAKE) -C $(APPLICATION_DIR) clean

compile_flags.txt: Makefile
	@echo "Generating compile_flags.txt"
	@echo $(CFLAGS) "-I.emscripten_cache/sysroot/include/"  | sed 's/ /\n/g' > compile_flags.txt
