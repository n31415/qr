// @ts-check
/** @type {import('vite').UserConfig} */
export default {
  publicDir: "public",
  esbuild: {
    supported: {
      "top-level-await": true,
    },
  },
  build: {
    emptyOutDir: true,
    outDir: "dist",
    rollupOptions: {
      input: {
        app: "./qr.html",
      },
    },
  },
  server: {
    open: "/qr.html",
  },
};
