NAME = qr
PROG = $(NAME)
CC = gcc
PKGS = gdk-3.0
CFLAGS = `pkg-config --cflags $(PKGS)` -std=gnu11 -Wall -Wextra -pedantic -Werror -O3 -march=native -g
LDFLAGS = `pkg-config --libs $(PKGS)` -lm -Wall -Wextra -pedantic -Werror -O3 -march=native
IMPL_SOURCES = rs.c qr.c
IMPL_HEADERS = rs.h qr.h
APP_SOURCES = application.c main.c
APP_HEADERS = application.h
SOURCES = $(IMPL_SOURCES) $(APP_SOURCES)
HEADERS = $(IMPL_HEADERS) $(APP_HEADERS)
OBJECTS = $(SOURCES:.c=.o)

WASM_NAME = qr_wasm
PROG_JS = $(WASM_NAME).js
PROG_WASM = $(WASM_NAME).wasm
WASM_MAIN = $(WASM_NAME).c
WASM_D_TS = $(WASM_MAIN:.c=.d.ts)
WASM_OBJ = $(IMPL_SOURCES:.c=_wasm.o)
WASM_CC = emcc
EMCC_CFLAGS = --cache $(CURDIR)/.emscripten_cache -std=c11 -Wall -Wextra -pedantic -Werror -O3 -msimd128 -flto
EMCC_LDFLAGS = -s WASM=1 -s EXPORTED_RUNTIME_METHODS=ccall -s MODULARIZE=1 -s EXPORT_NAME=createModule -s DYNAMIC_EXECUTION=0 -s DEFAULT_TO_CXX=0 -s EXPORT_ES6=1 -s ENVIRONMENT=web

.SUFFIXES:

.PHONY: all app clean wasm deploy ci serve

all: app wasm compile_flags.txt

app: $(PROG)

wasm: $(PROG_JS) $(WASM_D_TS)

deploy: $(DEPLOY_DIR_DUMMY_FILE)

$(PROG_WASM) $(PROG_JS) &: $(WASM_MAIN) $(WASM_OBJ)
	$(WASM_CC) $(EMCC_CFLAGS) $(EMCC_LDFLAGS) $(WASM_MAIN) $(WASM_OBJ) -o $@

%_wasm.o: %.c $(IMPL_HEADERS)
	$(WASM_CC) $(EMCC_CFLAGS) -c $< -o $@

$(PROG): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

%.o: %.c $(HEADERS)
	$(CC) -c $(CFLAGS) $< -o $@

compile_flags.txt: Makefile
	@echo "Generating compile_flags.txt"
	@echo $(CFLAGS) "-I.emscripten_cache/sysroot/include/"  | sed 's/ /\n/g' > compile_flags.txt

clean:
	rm -rf $(WASM_OBJ) $(OBJECTS) $(PROG) $(PROG_JS) $(PROG_WASM) $(DEPLOY_DIR) $(WASM_D_TS)

serve:
	$(BROWSER) http://localhost:8000/qr.html &
	python -m http.server

ci:
	git ls-tree -r HEAD --name-only | entr make

%.d.ts: %.c generate_types.sh
	./generate_types.sh $(EMCC_CFLAGS) -c $< > $@
