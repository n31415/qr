#!/bin/sh

arg_type_strings='"string" | "number" | "array"'
arg_types='string | number | Uint8Array'

functions=$(
	emcc -Xclang -ast-dump=json "$@" |
		jq '[.. |
        select(
            (.kind? == "FunctionDecl") and
            (.inner? // [] | any(.kind == "UsedAttr"))
        ) |
        ({name} + ( .type.qualType |
            capture("(?<ret>^[^(]*) *\\((?<args>.*)\\)") |
            { ret: .ret | (if . == "void" then "undefined" else "number" end),
              args: (.args | split(" *, *";null) |
                        map( if endswith("char *") then "string" else "number" end)
                    )
            }
            )
        )
    ]'
)

function_idents=$(echo "$functions" | jq -r 'map("\"\(.name)\"") | join(" | ")')

cat <<EOF
export class Module {
    onRuntimeInitialized: undefined | (() => Any);
	ccall(
		ident: $function_idents,
		ret_type: "string",
		arg_types: ($arg_type_strings)[],
		arg_values: ($arg_types)[],
	): string;
	ccall(
		ident: $function_idents,
		ret_type: "number",
		arg_types: ($arg_type_strings)[],
		arg_values: ($arg_types)[],
	): number;
	ccall(
                ident: $function_idents,
		ret_type: undefined | null,
		arg_types: ($arg_type_strings)[],
		arg_values: ($arg_types)[],
	): undefined;
	cwrap(
                ident: $function_idents,
		ret_type: string | undefined,
		arg_types: string[],
	): (arg_values: (string | number | Uint8Array)[]) => string | number | undefined;
EOF

echo "$functions" |
	jq 'map("\t_\(.name)(\(.args |
        [ foreach .[] as $item (0; . + 1; {index: ., $item})|
        "arg\(.index): \(.item)" ]|
        join(", ") )): \(.ret);"
) | join("\n")' --raw-output

cat <<EOF
}

export default function createModule(): Promise<Module>;
EOF
