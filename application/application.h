#ifndef APPLICATION_H
#define APPLICATION_H

#include <cairo/cairo.h>

typedef struct pimpl_application application_t;
typedef void (*application_button_handler_t)(application_t *app, unsigned int button, int down, double x, double y);
typedef void (*application_motion_handler_t)(application_t *app, double x, double y);
typedef void (*application_key_handler_t)(application_t *app, unsigned int keyval, int down);
typedef void (*application_update_function_t)(application_t *app, long time);
typedef void (*application_draw_function_t)(application_t *app, cairo_t *cr, int width, int height);

struct pimpl_application;

void application_init(int *argc, char ***argv);

application_t * application_create(char *title, int width, int height);
long application_get_time(application_t *app);

void application_set_button_handler(application_t *app, application_button_handler_t button_handler);
void application_set_motion_handler(application_t *app, application_motion_handler_t motion_handler);
//void application_set_scroll_handler(application_t *app, application_scroll_handler_t scroll_handler);
void application_set_key_handler(application_t *app, application_key_handler_t key_handler);
void application_set_update_function(application_t *app, application_update_function_t update_function);
void application_set_draw_function(application_t *app, application_draw_function_t draw_function);

void application_run(application_t *app);

void application_destroy(application_t *app);

#endif
