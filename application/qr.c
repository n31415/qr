#include "strings.h"
#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "qr.h"
#include "rs.h"

typedef struct {
    _Bool bright : 1;
    _Bool set : 1;
    _Bool by_user : 1;
} state;

const _Bool LIGHT = 0;
const _Bool DARK = 1;

static_assert(sizeof(state) == 1, "Pixel struct should be small");

#define MAX_VERSION      40
#define QR_SIZE(version) (4 * (version) + 17)

static_assert(QR_SIZE(MAX_VERSION) * QR_SIZE(MAX_VERSION) < 1U << 15U,
              "Should use something bigger than int16_t");

struct pimpl_qr {
    uint8_t version;
    uint8_t quality; /* L=1, M=0, Q=3, H=2 */
    uint8_t mask;    /* 0..7 */
    size_t n;        /* 4 * version + 17 */
    size_t stream_size;
    size_t data_size;
    size_t num_blocks;
    size_t data_per_block;
    state *pixel; /* light=0, dark=1, unset=-1 */
    int16_t *order;
    uint16_t *position;
    int16_t *data;
};

inline static size_t point_to_index(qr_t *qr, point p) {
    return qr->n * p.y + p.x;
}

inline static point index_to_point(qr_t *qr, size_t index) {
    return (point) {.x = index % qr->n, .y = index / qr->n};
}

inline static void auto_set(qr_t *qr, point p, int value) {
    qr->pixel[point_to_index(qr, p)] = (state) {.bright = value, .set = true, .by_user = false};
}

inline static void update(state *px, int value) {
    px->bright = value;
    if (!px->set && value >= 0) {
        px->set = true;
        px->by_user = false;
    }
    if (value < 0) {
        px->set = false;
        px->by_user = false;
    }
}

static void draw_square(qr_t *qr, size_t cx, size_t cy, size_t size, _Bool value) {
    const size_t start_x = cx >= size ? cx - size : 0;
    const size_t start_y = cy >= size ? cy - size : 0;

    for (size_t x = start_x; x <= cx + size; x++) {
        for (size_t y = start_y; y <= cy + size; y++) {
            if (x < qr->n && y < qr->n) {
                auto_set(qr, (point) {.x = x, .y = y}, value);
            }
        }
    }
}

static void draw_alignment(qr_t *qr, size_t x, size_t y) {
    draw_square(qr, x, y, 2, DARK);
    draw_square(qr, x, y, 1, LIGHT);
    draw_square(qr, x, y, 0, DARK);
}

static void draw_detection(qr_t *qr, size_t x, size_t y) {
    draw_square(qr, x, y, 4, LIGHT);
    draw_square(qr, x, y, 3, DARK);
    draw_square(qr, x, y, 2, LIGHT);
    draw_square(qr, x, y, 1, DARK);
}

static void draw_timing(qr_t *qr) {
    for (size_t x = 6; x < qr->n - 6; x++) {
        auto_set(qr, (point) {.x = x, .y = 6}, (int) (x + 1) % 2);
        auto_set(qr, (point) {.x = 6, .y = x}, (int) (x + 1) % 2);
    }
}

static void draw_patterns(qr_t *qr) {
    draw_detection(qr, 3, 3);
    draw_detection(qr, qr->n - 4, 3);
    draw_detection(qr, 3, qr->n - 4);
    draw_timing(qr);
    if (qr->version <= 1) {
        return;
    }
    size_t a = qr->version / 7 + 1;
    size_t asep = (qr->n - 14) / a + 1;
    for (size_t i = 1; i < a; i++) {
        assert(qr->n - 7 - i * asep > 0);
        draw_alignment(qr, qr->n - 7 - i * asep, 6);
        draw_alignment(qr, 6, qr->n - 7 - i * asep);
    }
    for (size_t i = 0; i < a; i++) {
        for (size_t j = 0; j < a; j++) {
            assert(qr->n - 7 - i * asep > 0);
            draw_alignment(qr, qr->n - 7 - i * asep, qr->n - 7 - j * asep);
        }
    }
}

static unsigned get_format(qr_t *qr) {
    unsigned f = ((unsigned) qr->quality << 3U) | (unsigned) qr->mask;
    unsigned x = f << 5U;
    for (int i = 0; i < 5; i++) {
        x <<= 1U;
        if (x & 0x400U) {
            x ^= 0x537U;
        }
    }
    x |= f << 10U;
    x ^= 0x5412U;
    return x;
}

static unsigned get_version(qr_t *qr) {
    unsigned v = qr->version;
    unsigned x = v << 6U;
    for (int i = 0; i < 6; i++) {
        x <<= 1U;
        if (x & 0x1000U) {
            x ^= 0x1f25U;
        }
    }
    x |= v << 12U;
    return x;
}

static void draw_meta(qr_t *qr) {
    size_t x = get_format(qr);
    for (size_t i = 0; i < 6; i++) {
        auto_set(qr, (point) {.x = 8, .y = i}, (int) ((x >> i) & 1U));
    }
    auto_set(qr, (point) {.x = 8, .y = 7}, (int) ((x >> 6U) & 1U));
    auto_set(qr, (point) {.x = 8, .y = 8}, (int) ((x >> 7U) & 1U));
    auto_set(qr, (point) {.x = 7, .y = 8}, (int) ((x >> 8U) & 1U));
    for (size_t i = 9; i < 15; i++) {
        auto_set(qr, (point) {.x = 14 - i, .y = 8}, (int) ((x >> i) & 1U));
    }
    for (size_t i = 0; i < 8; i++) {
        auto_set(qr, (point) {.x = qr->n - 1 - i, .y = 8}, (int) ((x >> i) & 1U));
    }
    auto_set(qr, (point) {.x = 8, .y = qr->n - 8}, 1);
    for (size_t i = 8; i < 15; i++) {
        auto_set(qr, (point) {.x = 8, .y = qr->n - 15 + i}, (int) ((x >> i) & 1U));
    }

    if (qr->version < 7) {
        return;
    }
    x = get_version(qr);
    for (size_t i = 0; i < 18; i++) {
        auto_set(qr, (point) {.x = i / 3, .y = qr->n - 11 + i % 3}, (int) ((x >> i) & 1U));
    }
    for (size_t i = 0; i < 18; i++) {
        auto_set(qr, (point) {.x = qr->n - 11 + i % 3, .y = i / 3}, (int) ((x >> i) & 1U));
    }
}

static void prepare_mapping(qr_t *qr) {
    for (size_t x = 0; x < qr->n; x++) {
        for (size_t y = 0; y < qr->n; y++) {
            qr->order[point_to_index(qr, (point) {.x = x, .y = y})] = -1;
        }
    }
    int dir = -1;
    int16_t k = 0;
    for (size_t x0 = qr->n; x0 > 1; x0 -= 2) {
        if (x0 == 7) {
            x0--;
        }
        for (size_t y = (dir > 0 ? 1 : qr->n); (dir > 0 ? y <= qr->n : y > 0); y += dir) {
            for (size_t delta_x = 0; delta_x < 2; ++delta_x) {
                size_t index = point_to_index(qr, (point) {.x = x0 - delta_x - 1, .y = y - 1});
                assert(x0 >= delta_x + 1);
                if (!qr->pixel[index].set) {
                    qr->position[k] = index;
                    qr->order[index] = k++;
                }
            }
        }
        dir = -dir;
    }
    qr->stream_size = k;
}

static uint8_t get_mask(uint8_t mask, point p) {
    switch (mask) {
    case 0: return (p.x + p.y) % 2 == 0;
    case 1: return p.y % 2 == 0;
    case 2: return p.x % 3 == 0;
    case 3: return (p.x + p.y) % 3 == 0;
    case 4: return (p.y / 2 + p.x / 3) % 2 == 0;
    case 5: return p.x * p.y % 2 + p.x * p.y % 3 == 0;
    case 6: return (p.x * p.y % 2 + p.x * p.y % 3) % 2 == 0;
    case 7: return ((p.x + p.y) % 2 + p.x * p.y % 3) % 2 == 0;
    default: return 0;
    }
}

int qr_sync(qr_t *qr) {
    int result = 1;

    uint8_t *stream = calloc(qr->stream_size, sizeof(*stream));
    size_t skip = qr->stream_size / 8 % qr->num_blocks;
    if (skip != 0) {
        skip = qr->num_blocks - skip;
    }
    size_t block_size = qr->stream_size / 8 / qr->num_blocks;
    int16_t **blocks = calloc(qr->num_blocks, sizeof(*blocks));
    for (size_t b = 0; b < qr->num_blocks; b++) {
        blocks[b] = calloc(((unsigned long) block_size + 1) * 8, sizeof(*blocks[b]));
        for (size_t j = 0; j < (block_size + 1) * 8; j++) {
            blocks[b][j] = -1;
        }
    }

    /* pixel -> stream */
    for (size_t i = 0; i < qr->stream_size; i++) {
        if (qr->pixel[qr->position[i]].set && qr->pixel[qr->position[i]].by_user) {
            stream[i] = qr->pixel[qr->position[i]].bright;
        } else {
            stream[i] = -1;
        }
        if (stream[i] != (uint8_t) -1) {
            stream[i] ^= get_mask(qr->mask, index_to_point(qr, qr->position[i]));
        }
    }
    /* stream -> blocks */
    for (size_t i = 0; i < qr->stream_size / 8 * 8; i++) {
        if (stream[i] == (uint8_t) -1) {
            continue;
        }
        size_t j = i / 8 / qr->num_blocks;
        size_t b = 0;
        if (j < qr->data_per_block) {
            b = i / 8 % qr->num_blocks;
        } else {
            b = (i / 8 + skip) % qr->num_blocks;
        }
        blocks[b][j * 8 + i % 8] = stream[i];
    }
    /* data -> blocks */
    for (size_t i = 0; i < qr->data_size; i++) {
        if (qr->data[i] < 0) {
            continue;
        }
        size_t b = i / 8 / qr->data_per_block;
        size_t j = i / 8 % qr->data_per_block;
        if (skip != 0 && b >= skip) {
            b = skip + (i / 8 - skip * qr->data_per_block) / (qr->data_per_block + 1);
            j = (i / 8 - skip * qr->data_per_block) % (qr->data_per_block + 1);
        }
        if (b < qr->num_blocks) {
            blocks[b][j * 8 + i % 8] = qr->data[i];
        }
    }
    /* error correction */
    for (size_t b = 0; b < qr->num_blocks; b++) {
        result = rs_fix(block_size + (skip != 0 && b >= skip), block_size - qr->data_per_block,
                        blocks[b]) &&
                 result;
    }
    /* blocks -> data */
    for (size_t i = 0; i < qr->data_size; i++) {
        size_t b = i / 8 / qr->data_per_block;
        size_t j = i / 8 % qr->data_per_block;
        if (skip != 0 && b >= skip) {
            b = skip + (i / 8 - skip * qr->data_per_block) / (qr->data_per_block + 1);
            j = (i / 8 - skip * qr->data_per_block) % (qr->data_per_block + 1);
        }
        if (b < qr->num_blocks) {
            qr->data[i] = blocks[b][j * 8 + i % 8];
        } else {
            qr->data[i] = -1;
        }
    }
    /* blocks -> stream */
    for (size_t i = 0; i < qr->stream_size / 8 * 8; i++) {
        size_t j = i / 8 / qr->num_blocks;
        size_t b = 0;
        if (j < qr->data_per_block) {
            b = i / 8 % qr->num_blocks;
        } else {
            b = (i / 8 + skip) % qr->num_blocks;
        }
        stream[i] = blocks[b][j * 8 + i % 8];
    }
    /* stream -> pixel */
    for (size_t i = 0; i < qr->stream_size; i++) {
        if (stream[i] != (uint8_t) -1) {
            stream[i] ^= get_mask(qr->mask, index_to_point(qr, qr->position[i]));
            update(qr->pixel + qr->position[i], stream[i]);
        } else {
            update(qr->pixel + qr->position[i], -1);
        }
    }
    free(stream);
    for (size_t b = 0; b < qr->num_blocks; b++) {
        free(blocks[b]);
    }
    free(blocks);

    return result;
}

void qr_set_bytes(qr_t *qr, size_t len, const unsigned char *bytes) {
    size_t len_bits = (qr->version < 10) ? 8 : 16;
    qr->data_size = 4 + len_bits + len * 8 + 4;
    /* TODO: check if qr->data_size is too large */
    qr->data = calloc(qr->data_size, sizeof(*qr->data));
    qr->data[0] = 0;
    qr->data[1] = 1;
    qr->data[2] = 0;
    qr->data[3] = 0;
    for (size_t i = 0; i < len_bits; i++) {
        qr->data[4 + i] = (int16_t) ((len >> (len_bits - 1 - i)) & 1U);
    }
    for (size_t i = 0; i < len * 8; i++) {
        unsigned remainder = 7 - i % 8U;
        qr->data[4 + len_bits + i] = (int16_t) ((uint8_t) (bytes[i / 8U] >> remainder) & 1U);
    }
    qr->data[qr->data_size - 4] = 0;
    qr->data[qr->data_size - 3] = 0;
    qr->data[qr->data_size - 2] = 0;
    qr->data[qr->data_size - 1] = 0;
}

int qr_get_pixel(qr_t *qr, point p) {
    if (p.x >= qr->n || p.y >= qr->n) {
        return -1;
    }
    size_t index = point_to_index(qr, p);

    if (qr->pixel[index].set) {
        return qr->pixel[index].bright;
    }

    return -1;
}

static int qr_set_pixel_by_index(qr_t *qr, size_t index, int p) {
    if (qr->order[index] < 0 || (qr->pixel[index].bright == p && qr->pixel[index].set)) {
        return 0;
    }

    if (!qr->pixel[index].by_user && qr->pixel[index].set) {
        // The pixel is not unset, but was not set by user, hence we may not override it.
        return 0;
    }

    qr->pixel[index] = (state) {.bright = p, .set = p >= 0, .by_user = true};

    return 1;
}

int qr_set_pixel(qr_t *qr, point point, int p) {
    if (point.x >= qr->n || point.y >= qr->n || p > 1) {
        return 0;
    }
    size_t index = point_to_index(qr, point);
    return qr_set_pixel_by_index(qr, index, p);
}

size_t qr_get_n(qr_t *qr) {
    return qr->n;
}

size_t qr_set_many_checked(qr_t *qr, size_t num_pixel, pixel pixels[num_pixel]) {
    size_t lower_bound = 0;
    size_t upper_bound = num_pixel;

    size_t drawn_until = 0;

    while (true) {
        size_t middle = (lower_bound + upper_bound + 1) / 2;

        while (drawn_until < middle) {
            qr_set_pixel(qr, pixels[drawn_until].point, pixels[drawn_until].value);
            drawn_until++;
        }
        while (drawn_until > middle) {
            qr->pixel[point_to_index(qr, pixels[drawn_until].point)] = (state) {0};
            drawn_until--;
        }

        if (qr_sync(qr)) {
            if (lower_bound == middle) {
                break;
            }
            lower_bound = middle;
        } else {
            if (upper_bound == middle) {
                break;
            }
            upper_bound = middle;
        }
    }

    return drawn_until;
}

bool is_valid(qr_t *qr, point p) {
    return (p.x + 1 > 0 && p.x < qr->n && p.y + 1 > 0 && p.y < qr->n);
}

size_t run_bfs(qr_t *qr, pixel *to_be_filled, fillFunc method) {
    const size_t num_pixels = qr->n * qr->n;

    size_t *const bfs_order = calloc(num_pixels, sizeof(*bfs_order));
    uint8_t *const already_queued = calloc(num_pixels, sizeof(*already_queued));

    size_t num_bfs = 0;
    size_t num_visited = 0;
    size_t num_to_be_filled = 0;

    for (size_t index = 0; index < num_pixels; ++index) {
        if (qr->pixel[index].set && qr->pixel[index].by_user) {
            bfs_order[num_bfs++] = index;
            already_queued[index] = true;
        }
    }

    if (num_bfs == 0) {
        size_t index = point_to_index(qr, (point) {.x = qr->n / 2, .y = qr->n / 2});
        bfs_order[num_bfs++] = index;
        already_queued[index] = true;
    }

    while (num_visited < num_bfs) {
        const size_t next = bfs_order[num_visited++];
        point p = index_to_point(qr, next);

        if (!qr->pixel[next].set) {
            to_be_filled[num_to_be_filled++] = (pixel) {.point = p, .value = method(p) % 2};
        }

        for (int d = -1; d < 2; d += 2) {
            point new_point = {.x = p.x + d, .y = p.y};
            if (is_valid(qr, new_point)) {
                size_t index = point_to_index(qr, new_point);

                if (!already_queued[index]) {
                    bfs_order[num_bfs++] = index;
                    already_queued[index] = true;
                }
            }
            new_point = (point) {.x = p.x, .y = p.y + d};
            if (is_valid(qr, new_point)) {
                size_t index = point_to_index(qr, new_point);

                if (!already_queued[index]) {
                    bfs_order[num_bfs++] = index;
                    already_queued[index] = true;
                }
            }
        }
    }

    free(already_queued);
    free(bfs_order);

    return num_to_be_filled;
}

#define FILL_BATCH 512

void qr_fill(qr_t *qr, fillFunc method) {
    const size_t num_pixels = qr->n * qr->n;
    pixel *const to_be_filled = calloc(num_pixels, sizeof(*to_be_filled));

    const size_t num_to_be_filled = run_bfs(qr, to_be_filled, method);

    size_t backtrack_buffer[FILL_BATCH];
    size_t cur_backtrack_index = 0;
    size_t fill_stride = FILL_BATCH;
    // We could also retry this binary search, but the performance sucks.
    size_t todo = qr_set_many_checked(qr, num_to_be_filled, to_be_filled);
    size_t synced_upto = 0;

    while (todo < num_to_be_filled) {
        const pixel cur = to_be_filled[todo++];
        const size_t index = point_to_index(qr, cur.point);

        if (qr->pixel[index].set || !qr_set_pixel_by_index(qr, index, cur.value)) {
            continue;
        }

        backtrack_buffer[cur_backtrack_index++] = index;
        if (cur_backtrack_index >= fill_stride || todo == num_to_be_filled) {
            // We need to sync again
            if (!qr_sync(qr)) {
                if (fill_stride <= 1) {
                    qr->pixel[index].bright = 1 - qr->pixel[index].bright;
                } else {
                    fill_stride /= 2;
                    for (size_t i = 0; i < cur_backtrack_index; ++i) {
                        qr->pixel[backtrack_buffer[i]] = (state) {0};
                    }
                    todo = synced_upto;
                }
            }

            synced_upto = todo;
            cur_backtrack_index = 0;
        }
    }

    free(to_be_filled);
}

const int num_blocks[MAX_VERSION * 4] = {
    1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  1,  2,  2,  4,  1,  2,  4,  4,  2,  4,  4,
    4,  2,  4,  6,  5,  2,  4,  6,  6,  2,  5,  8,  8,  4,  5,  8,  8,  4,  5,  8,  11, 4,  8,
    10, 11, 4,  9,  12, 16, 4,  9,  16, 16, 6,  10, 12, 18, 6,  10, 17, 16, 6,  11, 16, 19, 6,
    13, 18, 21, 7,  14, 21, 25, 8,  16, 20, 25, 8,  17, 23, 25, 9,  17, 23, 34, 9,  18, 25, 30,
    10, 20, 27, 32, 12, 21, 29, 35, 12, 23, 34, 37, 12, 25, 34, 40, 13, 26, 35, 42, 14, 28, 38,
    45, 15, 29, 40, 48, 16, 31, 43, 51, 17, 33, 45, 54, 18, 35, 48, 57, 19, 37, 51, 60, 19, 38,
    53, 63, 20, 40, 56, 66, 21, 43, 59, 70, 22, 45, 62, 74, 24, 47, 65, 77, 25, 49, 68, 81};

const int data_per_block[MAX_VERSION * 4] = {
    19,  16, 13, 9,  34,  28, 22, 16, 55,  44, 17, 13, 80,  32, 24, 9,  108, 43, 15, 11,
    68,  27, 19, 15, 78,  31, 14, 13, 97,  38, 18, 14, 116, 36, 16, 12, 68,  43, 19, 15,
    81,  50, 22, 12, 92,  36, 20, 14, 107, 37, 20, 11, 115, 40, 16, 12, 87,  41, 24, 12,
    98,  45, 19, 15, 107, 46, 22, 14, 120, 43, 22, 14, 113, 44, 21, 13, 107, 41, 24, 15,
    116, 42, 22, 16, 111, 46, 24, 13, 121, 47, 24, 15, 117, 45, 24, 16, 106, 47, 24, 15,
    114, 46, 22, 16, 122, 45, 23, 15, 117, 45, 24, 15, 116, 45, 23, 15, 115, 47, 24, 15,
    115, 46, 24, 15, 115, 46, 24, 15, 115, 46, 24, 15, 115, 46, 24, 16, 121, 47, 24, 15,
    121, 47, 24, 15, 122, 46, 24, 15, 122, 46, 24, 15, 117, 47, 24, 15, 118, 47, 24, 15};

qr_t *qr_create(size_t version, size_t quality, size_t mask) {
    if (version < 1 || version > MAX_VERSION || quality > 3 || mask > 7) {
        return NULL;
    }
    qr_t *qr = calloc(1, sizeof(*qr));
    qr->version = version;
    qr->quality = quality;
    qr->mask = mask;
    qr->n = QR_SIZE(version);
    qr->pixel = calloc(qr->n * qr->n, sizeof(*qr->pixel));
    qr->order = calloc(qr->n * qr->n, sizeof(*qr->order));
    qr->position = calloc(qr->n * qr->n, sizeof(*qr->position));
    size_t i = (version - 1) * 4 + (qr->quality ^ 1U);
    qr->num_blocks = num_blocks[i];
    qr->data_per_block = data_per_block[i];
    draw_patterns(qr);
    draw_meta(qr);
    prepare_mapping(qr);
    return qr;
}

void qr_destroy(qr_t *qr) {
    free(qr->pixel);
    free(qr->order);
    free(qr->position);
    free(qr->data);
    free(qr);
}
