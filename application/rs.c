#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "rs.h"

static void prepare_powers(size_t *powers) {
    size_t a = 1;
    for (size_t i = 0; i < 255; i++) {
        powers[i] = a;
        a <<= 1U;
        if (a & 0x100U) {
            a ^= 0x11dU;
        }
    }
}

struct ls {
    size_t m, n;
    uint8_t **a;
    uint8_t *b;
};

static void add_row(struct ls *ls, size_t i, size_t i0) {
    for (size_t j = 0; j < ls->n; j++) {
        ls->a[i][j] ^= ls->a[i0][j];
    }
    ls->b[i] ^= ls->b[i0];
}

static void swap_rows(struct ls *ls, size_t i, size_t i0) {
    for (size_t j = 0; j < ls->n; j++) {
        int tmp = ls->a[i][j];
        ls->a[i][j] = ls->a[i0][j];
        ls->a[i0][j] = tmp;
    }
    int tmp = ls->b[i];
    ls->b[i] = ls->b[i0];
    ls->b[i0] = tmp;
}

static int solve_ls(struct ls *ls, int16_t *x) {
    size_t k = 0;
    for (size_t j = 0; j < ls->n; j++) {
        int found = 0;
        for (size_t i = k; i < ls->m; i++) {
            if (ls->a[i][j]) {
                if (found) {
                    add_row(ls, i, k);
                } else {
                    swap_rows(ls, i, k);
                    found = 1;
                }
            }
        }
        if (found) {
            for (size_t i = 0; i < k; i++) {
                if (ls->a[i][j]) {
                    add_row(ls, i, k);
                }
            }
            k++;
        }
    }
    for (size_t i = k; i < ls->m; i++) {
        if (ls->b[i]) {
            return 0;
        }
    }
    for (size_t j = 0; j < ls->n; j++) {
        x[j] = -1;
    }
    for (size_t i = 0; i < k; i++) {
        int found = -1;
        for (size_t j = 0; j < ls->n; j++) {
            if (ls->a[i][j]) {
                if (found >= 0) {
                    found = -1;
                    break;
                }
                found = (int) j;
            }
        }
        if (found >= 0) {
            x[found] = ls->b[i];
        }
    }
    return 1;
}

static void free_ls(struct ls *ls) {
    for (size_t i = 0; i < ls->m; i++) {
        free(ls->a[i]);
    }
    free(ls->a);
    free(ls->b);
}

static void reverse(size_t n, int16_t *bits) {
    for (size_t i = 0; i < n / 2; i++) {
        int16_t tmp = bits[i];
        bits[i] = bits[n - 1 - i];
        bits[n - 1 - i] = tmp;
    }
}

int rs_fix(size_t n, size_t k, int16_t *bits) {
    size_t powers[255];
    struct ls ls;

    prepare_powers(powers);
    ls.m = k * 8;
    ls.n = 0;
    ls.a = calloc(k * 8, sizeof(*ls.a));
    ls.b = calloc(k * 8, sizeof(*ls.b));
    reverse(n * 8, bits);
    for (size_t i = 0; i < k * 8; i++) {
        ls.a[i] = calloc(n * 8, sizeof(*ls.a[i]));
        size_t l = 0;
        for (size_t j = 0; j < n * 8; j++) {
            uint8_t c = (powers[(i / 8 * (j / 8) + j % 8) % 255] >> (i % 8)) & 1U;
            if (bits[j] < 0) {
                ls.a[i][l++] = c;
            } else {
                ls.b[i] ^= (uint8_t) (c * bits[j]);
            }
        }
        ls.n = l;
    }

    int16_t *x = calloc(ls.n, sizeof(*x));
    if (solve_ls(&ls, x) != 1) {
        reverse(n * 8, bits);
        free_ls(&ls);
        free(x);
        return 0;
    }
    for (size_t j = 0, l = 0; j < n * 8; j++) {
        if (bits[j] < 0) {
            bits[j] = x[l++];
        }
    }
    reverse(n * 8, bits);
    free_ls(&ls);
    free(x);
    return 1;
}
