#include <emscripten.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "emscripten/em_macros.h"
#include "qr.h"

EMSCRIPTEN_KEEPALIVE
int set_pixel(qr_t *qr, int state, int x, int y) {
    point p = {.x = x, .y = y};
    int old_pixel = qr_get_pixel(qr, p);
    if (qr_set_pixel(qr, p, state)) {
        if (!qr_sync(qr) && old_pixel >= 0) {
            qr_set_pixel(qr, p, old_pixel);
            return false;
        }
        return true;
    }
    return false;
}

EMSCRIPTEN_KEEPALIVE
int set_pixel_unsynced(qr_t *qr, int state, int x, int y) {
    return qr_set_pixel(qr, (point) {.x = x, .y = y}, state);
}

EMSCRIPTEN_KEEPALIVE
int sync(qr_t *qr) {
    return qr_sync(qr);
}

EMSCRIPTEN_KEEPALIVE
int get_pixel(qr_t *qr, int x, int y) {
    return qr_get_pixel(qr, (point) {.x = x, .y = y});
}

EMSCRIPTEN_KEEPALIVE
size_t get_size(qr_t *qr) {
    return qr_get_n(qr);
}

int black_filler(point p) {
    (void) p;
    return 1;
}

int white_filler(point p) {
    (void) p;
    return 0;
}

int random_filler(point p) {
    (void) p;
    return rand();
}

int checkerboad_filler(point p) {
    // The + 1 makes it look nicer with the timing bands
    return (int) ((p.x + p.y + 1) % 2);
}

EMSCRIPTEN_KEEPALIVE
int fill(qr_t *qr, char *method) {
    if (strcmp(method, "black") == 0) {
        qr_fill(qr, black_filler);
        return 1;
    }
    if (strcmp(method, "white") == 0) {
        qr_fill(qr, white_filler);
        return 1;
    }
    if (strcmp(method, "random") == 0) {
        // It just shouldn't be completely deterministic
        srand((size_t) method ^ (size_t) qr);
        qr_fill(qr, random_filler);
        return 1;
    }
    if (strcmp(method, "checkerboard") == 0) {
        qr_fill(qr, checkerboad_filler);
        return 1;
    }

    return 0;
}

EMSCRIPTEN_KEEPALIVE
qr_t *init_qr(int version, int quality, int mask, unsigned char *data) {
    qr_t *qr = qr_create(version, quality, mask);

    if (qr == NULL) {
        return NULL;
    }

    if (data != NULL) {
        unsigned long len = strlen((const char *) data);
        qr_set_bytes(qr, len, data);
    }

    qr_sync(qr);

    return qr;
}

EMSCRIPTEN_KEEPALIVE
void destroy_qr(qr_t *qr) {
    qr_destroy(qr);
}

EMSCRIPTEN_KEEPALIVE
size_t draw_many_pixels(qr_t *qr, size_t num_pixels, const uint8_t pixel_buffer[3 * num_pixels]) {
    pixel *const pixels = calloc(num_pixels, sizeof(*pixels));

    for (size_t index = 0; index < num_pixels; ++index) {
        const uint8_t *buffer_slice = pixel_buffer + 3 * index;
        pixels[index] = (pixel) {
            .point.x = buffer_slice[0],
            .point.y = buffer_slice[1],
            .value = buffer_slice[2] % 2,
        };
    }

    const size_t result = qr_set_many_checked(qr, num_pixels, pixels);
    free(pixels);

    return result;
}
