#ifndef QR_H
#define QR_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
    size_t x;
    size_t y;
} point;

typedef struct {
    point point;
    uint8_t value;
} pixel;

struct pimpl_qr;
typedef struct pimpl_qr qr_t;

typedef int (*fillFunc)(point);

qr_t *qr_create(size_t version, size_t quality, size_t mask);
void qr_destroy(qr_t *qr);
int qr_sync(qr_t *qr);
void qr_set_bytes(qr_t *qr, size_t len, const unsigned char *bytes);
int qr_get_pixel(qr_t *qr, point point);
int qr_set_pixel(qr_t *qr, point point, int p);
size_t qr_get_n(qr_t *qr);
void qr_fill(qr_t *qr, fillFunc method);
size_t qr_set_many_checked(qr_t *qr, size_t num_pixel, pixel pixels[num_pixel]);

#endif
