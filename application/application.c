#include <gdk/gdk.h>

#include "application.h"

struct pimpl_application {
    GdkWindow *window;
    GdkFrameClock *clock;
    GMainLoop *loop;
    int width, height;

    application_button_handler_t button_handler;
    application_motion_handler_t motion_handler;
    application_key_handler_t key_handler;
    application_update_function_t update_function;
    application_draw_function_t draw_function;
};

static void event_handler(GdkEvent *event, gpointer data) {
    application_t *app = data;
    if (gdk_event_get_window(event) != app->window) {
        // printf("wrong window\n");
        return;
    }
    switch (gdk_event_get_event_type(event)) {
        case GDK_DESTROY: {
            // TODO
            break;
        }
        case GDK_DELETE: {
            g_main_loop_quit(app->loop);
            break;
        }
        case GDK_EXPOSE: {
            GdkDrawingContext *ctx = gdk_window_begin_draw_frame(app->window, event->expose.region);
            cairo_t *cr = gdk_drawing_context_get_cairo_context(ctx);
            if (app->draw_function != NULL) {
                app->draw_function(app, cr, app->width, app->height);
            }
            gdk_window_end_draw_frame(app->window, ctx);
            break;
        }
        case GDK_MOTION_NOTIFY: {
            if (app->motion_handler != NULL) {
                app->motion_handler(app, event->motion.x, event->motion.y);
            }
            gdk_event_request_motions(&event->motion);
            break;
        }
        case GDK_BUTTON_PRESS: {
            if (app->button_handler != NULL) {
                app->button_handler(app, event->button.button, 1, event->button.x, event->button.y);
            }
            break;
        }
        case GDK_BUTTON_RELEASE: {
            if (app->button_handler != NULL) {
                app->button_handler(app, event->button.button, 0, event->button.x, event->button.y);
            }
            break;
        }
        case GDK_SCROLL: {
            if (app->button_handler != NULL && event->scroll.direction != GDK_SCROLL_SMOOTH) {
                app->button_handler(app, 4 + event->scroll.direction, 1, event->scroll.x, event->scroll.y);
            }
            break;
        }
        case GDK_KEY_PRESS: {
            if (app->key_handler != NULL) {
                app->key_handler(app, event->key.keyval, 1);
            }
            break;
        }
        case GDK_KEY_RELEASE: {
            if (app->key_handler != NULL) {
                app->key_handler(app, event->key.keyval, 0);
            }
            break;
        }
        case GDK_CONFIGURE: {
            app->width = event->configure.width;
            app->height = event->configure.height;
            break;
        }
        default: break;
    }
}

static void update_handler(GdkFrameClock *clock, gpointer data) {
    (void) clock;

    application_t *app = data;
    if (app->update_function != NULL) {
        app->update_function(app, gdk_frame_clock_get_frame_time(app->clock));
    }
    // region = gdk_window_get_clip_region(app->window);
    cairo_region_t *region = gdk_window_get_visible_region(app->window);
    gdk_window_invalidate_region(app->window, region, FALSE);
    cairo_region_destroy(region);
}

void application_init(int *argc, char ***argv) {
    gdk_init(argc, argv);
}

application_t *application_create(char *title, int width, int height) {
    GdkWindowAttr attr;

    application_t *app = malloc(sizeof(*app));
    app->width = width;
    app->height = height;
    app->button_handler = NULL;
    app->motion_handler = NULL;
    app->key_handler = NULL;
    app->update_function = NULL;
    app->draw_function = NULL;
    gdk_event_handler_set(event_handler, app, NULL);  // FIXME: global; overwritten for multiple apps
    GdkDisplay *display = gdk_display_get_default();
    attr.title = title;
    attr.event_mask = GDK_ALL_EVENTS_MASK;
    attr.width = width;
    attr.height = height;
    attr.wclass = GDK_INPUT_OUTPUT;
    attr.window_type = GDK_WINDOW_TOPLEVEL;
    attr.visual = gdk_screen_get_rgba_visual(gdk_display_get_default_screen(display));
    attr.cursor = gdk_cursor_new_from_name(display, "default");
    attr.type_hint = GDK_WINDOW_TYPE_HINT_NORMAL;
    app->window = gdk_window_new(NULL, &attr, GDK_WA_TITLE | GDK_WA_VISUAL | GDK_WA_CURSOR | GDK_WA_TYPE_HINT);
    app->clock = gdk_window_get_frame_clock(app->window);
    g_signal_connect(app->clock, "update", G_CALLBACK(update_handler), app);
    app->loop = g_main_loop_new(NULL, TRUE);
    return app;
}

long application_get_time(application_t *app) {
    return gdk_frame_clock_get_frame_time(app->clock);
}

void application_set_button_handler(application_t *app, application_button_handler_t button_handler) {
    app->button_handler = button_handler;
}

void application_set_motion_handler(application_t *app, application_motion_handler_t motion_handler) {
    app->motion_handler = motion_handler;
}

// void application_set_scroll_handler(application_t *app, application_scroll_handler_t scroll_handler);

void application_set_key_handler(application_t *app, application_key_handler_t key_handler) {
    app->key_handler = key_handler;
}

void application_set_update_function(application_t *app, application_update_function_t update_function) {
    app->update_function = update_function;
}

void application_set_draw_function(application_t *app, application_draw_function_t draw_function) {
    app->draw_function = draw_function;
}

void application_run(application_t *app) {
    gdk_window_show(app->window);
    gdk_frame_clock_begin_updating(app->clock);
    g_main_loop_run(app->loop);
    gdk_frame_clock_end_updating(app->clock);
}

void application_destroy(application_t *app) {
    g_main_loop_unref(app->loop);
    gdk_window_destroy(app->window);
    free(app);
}
