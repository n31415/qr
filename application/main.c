#include <gdk/gdkkeysyms.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "qr.h"
#include "application.h"

application_t *app;
qr_t *qr;
cairo_matrix_t matrix;
long button_down = -1;
int rotation = 0;

static void draw(application_t *app, cairo_t *cr, int width, int height) {
    (void) app;
    double pixel_size;

    cairo_set_source_rgb(cr, 0.8, 0.8, 0.8);
    cairo_paint(cr);
    cairo_save(cr);
    size_t n = qr_get_n(qr);
    pixel_size = 0.8 * ((width < height) ? width : height) / n;
    cairo_translate(cr, 0.5 * width, 0.5 * height);
    cairo_rotate(cr, rotation % 4 * M_PI_2);
    cairo_scale(cr, pixel_size, pixel_size);
    cairo_translate(cr, -0.5 * n, -0.5 * n);
    cairo_get_matrix(cr, &matrix);
    cairo_matrix_invert(&matrix);
    for (size_t x = 0; x < n; x++) {
        for (size_t y = 0; y < n; y++) {
            int px = qr_get_pixel(qr, (point){.x = x, .y = y});
            if (px < 0) {
                cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
            } else if (px) {
                cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
            } else {
                cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
            }
            cairo_rectangle(cr, (double) x, (double) y, 1, 1);
            cairo_fill(cr);
        }
    }
    cairo_restore(cr);
}

static void on_motion(application_t *app, double x, double y) {
    (void) app;
    int p;

    if (button_down < 0) {
        return;
    }
    cairo_matrix_transform_point(&matrix, &x, &y);
    if (button_down == 1) {
        p = 1;
    } else if (button_down == 3) {
        p = 0;
    } else if (button_down == 2) {
        p = -1;
    } else {
        return;
    }
    if (qr_set_pixel(qr, (point){.x = x, .y = y}, p)) {
        qr_sync(qr);
    }
}

static void on_button(application_t *app, unsigned int button, int down, double x, double y) {
    if (down) {
        button_down = button;
        on_motion(app, x, y);
    } else {
        button_down = -1;
    }
}

static void on_key(application_t *app, unsigned int keyval, int down) {
    (void) app;
    if (!down) {
        return;
    }
    if (keyval == GDK_KEY_bracketleft) {
        rotation--;
        return;
    }
    if (keyval == GDK_KEY_bracketright) {
        rotation++;
        return;
    }
}

int main(int argc, char **argv) {
    int version, quality, mask;

    application_init(&argc, &argv);
    cairo_matrix_init_identity(&matrix);

    if (argc < 4 || sscanf(argv[1], "%d", &version) < 1 || sscanf(argv[2], "%d", &quality) < 1 ||
        sscanf(argv[3], "%d", &mask) < 1) {
        fprintf(stderr, "Usage: %s version quality mask [data]\n", argv[0]);
        return 1;
    }

    qr = qr_create(version, quality, mask);
    if (qr == NULL) {
        fprintf(stderr, "Failed to create QR code with specified parameters.\n");
        return 1;
    }
    if (argc > 4) {
        qr_set_bytes(qr, strlen(argv[4]), (unsigned char *) argv[4]);
    }
    qr_sync(qr);

    app = application_create("qr", 1280, 720);
    application_set_draw_function(app, draw);
    application_set_button_handler(app, on_button);
    application_set_motion_handler(app, on_motion);
    application_set_key_handler(app, on_key);
    application_run(app);
    application_destroy(app);

    qr_destroy(qr);
    return 0;
}
