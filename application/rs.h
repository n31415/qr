#ifndef RS_H
#define RS_H

#include <stddef.h>
#include <stdint.h>

int rs_fix(size_t n, size_t k, int16_t *bits);

#endif
